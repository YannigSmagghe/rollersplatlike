﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{

    public Rigidbody rig;

    public float speed;

    private bool isTraveling;

    private Vector3 travelDirection;
    private Vector3 nextColisionPosition;

    public int minSwipeRecognition = 500; // 5 millimeters

    private Vector2 swipePositionLastFrame;
    private Vector2 swipePositionCurrentFrame;
    private Vector2 currentSwipe;

    //Change color
    private Color solveColor;

    // Start is called before the first frame update

    private void Start()
    {
        rig = GetComponent<Rigidbody>();
        solveColor = Random.ColorHSV(0.5f, 1);
        GetComponent<MeshRenderer>().material.color = solveColor;
    }

    void FixedUpdate()
    {
        if (isTraveling)
        {
            rig.velocity = travelDirection * speed;
        }

        // Check bot sphere and distance to check
        Collider[] hitColliders = Physics.OverlapSphere(transform.position - (Vector3.up / 2), 0.05f);
        int i = 0;
        while (i < hitColliders.Length)
        {
            GroundPiece ground = hitColliders[i].transform.GetComponent<GroundPiece>();
            if (ground && !ground.isColored)
            {
                ground.ChangeColor(solveColor);
            }

            i++;

        }

        if (nextColisionPosition != Vector3.zero)
        {
            if (Vector3.Distance(transform.position, nextColisionPosition) < 1)
            {
                isTraveling = false;
                travelDirection = Vector3.zero;
                nextColisionPosition = Vector3.zero;

            }

        }

        if (isTraveling)
            return;

        if (Input.GetMouseButton(0))
        {
            swipePositionCurrentFrame = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            if (swipePositionLastFrame != Vector2.zero)
            {
                currentSwipe = swipePositionCurrentFrame - swipePositionLastFrame;

                // check if swipe is enough to valide move
                if (currentSwipe.sqrMagnitude < minSwipeRecognition)
                {
                    return;
                }

                currentSwipe.Normalize();

                // UP / DOWN
                if (currentSwipe.x > -0.5f && currentSwipe.x < 0.5)
                {
                    // GO Up/Down
                    SetDestination(currentSwipe.y > 0 ? Vector3.forward : Vector3.back);
                }

                // LEFT / RIGHT
                if (currentSwipe.y > -0.5f && currentSwipe.y < 0.5)
                {
                    // GO Left/Right
                    SetDestination(currentSwipe.x > 0 ? Vector3.right : Vector3.left);
                }
            }

            swipePositionLastFrame = swipePositionCurrentFrame;
        }

        if (Input.GetMouseButtonUp(0))
        {
            swipePositionLastFrame = Vector2.zero;
            currentSwipe = Vector2.zero;
        }
    }

    private void SetDestination(Vector3 direction)
    {
        travelDirection = direction;

        RaycastHit hit;
        if (Physics.Raycast(transform.position, direction, out hit, 100f))
        {
            nextColisionPosition = hit.point;
        }

        isTraveling = true;
    }
}
